package Decorator;

public abstract class BeverageDecorator extends Beverage {
	public abstract String getDescription();
}
