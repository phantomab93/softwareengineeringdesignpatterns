package Decorator;

public class Mocha extends BeverageDecorator {
	
	Beverage beverage;
	
	public Mocha(Beverage bevarage){
		this.beverage = bevarage;
	}
	
	
	@Override
	public String getDescription() {
		return beverage.getDescription() + " with Mocha ";
	}

	@Override
	public double cost() {
		return beverage.cost()+.20;
	}

}
