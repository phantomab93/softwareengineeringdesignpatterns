package Decorator;

public class Whip extends BeverageDecorator {

	Beverage beverage;
	
	public Whip(Beverage beverage){
		this.beverage = beverage;
	}
	
	@Override
	public String getDescription() {
		return beverage.getDescription() + " , Whip cream ";
	}

	@Override
	public double cost() {
		return beverage.cost()+.30;
	}

}
