package Decorator;

public class Starbucks {

	public static void main(String[] args) {
		Beverage beverage = new Espresso();
		System.out.println("Description: "+ beverage.getDescription() + " Cost: "+ beverage.cost());
		
		beverage = new Mocha(beverage);
		System.out.println("Description: "+ beverage.getDescription() + "Cost: " +beverage.cost());
		
		beverage = new Whip(beverage);
		System.out.println("Description: "+ beverage.getDescription() + "Cost: " +beverage.cost());
		
	}

}
