package ObserverExample;

import java.util.ArrayList;
import java.util.List;

public class StatisticsDisplay implements Observer, DisplayElement {

	List<WeatherElements>forcastHistory = new ArrayList<WeatherElements>();
	private float temp;
	private float humidity;
	private float pressure;
	private float avgtemp;
	private float avghum;
	private float avgpre;
	private Subject weatherData;
	
	public StatisticsDisplay(Subject weatherData)
	{
		this.weatherData = weatherData;
		
		weatherData.registerObserver(this);
	}
	

	@Override
	public void display() {
	System.out.println("Avg.temp: " + avgtemp +" F° "+  " Avg.Humidity: "+ avghum + "% Avg.pressure: " + avgpre);	

	}

	@Override
	public void update(float temp, float humidity, float pressure) {
		WeatherElements element = new WeatherElements(temp,humidity,pressure);
		forcastHistory.add(element);
		averageCalculator(element);
		display();
	}

	private void averageCalculator(WeatherElements element) {
		temp += element.getTemp();
		 avgtemp	= temp/forcastHistory.size();
		humidity += element.getHumidity();
		 avghum 	= humidity/forcastHistory.size();
		pressure += element.getPressure();
		 avgpre 	=pressure/forcastHistory.size();
		
	}

}
