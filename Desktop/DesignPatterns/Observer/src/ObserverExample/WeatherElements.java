package ObserverExample;

public class WeatherElements {
	private float temp;
	private float humidity;
	private float pressure; 
	
	
	public WeatherElements(float temp, float humidity,float pressure){
		this.temp= temp;
		this.humidity = humidity;
		this.pressure = pressure;
	}
	
	public float getTemp(){
		return temp;
		
	}
	
	public float getHumidity(){
		return humidity;
	}

	public float getPressure(){
		return pressure;
	}
	
	
}
