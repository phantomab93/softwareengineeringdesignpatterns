package ObserverExample;

public class CurrentConditions implements Observer, DisplayElement {
	private float temp;
	private float humidity;
	private float pressure;
	private Subject weatherData;
	
	
	public CurrentConditions(Subject weatherData){
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
		
	}
	
	@Override
	public void update(float temp, float humidity, float pressure) {
		this.temp = temp;
		this.humidity =humidity;
		this.pressure = pressure;
		display();

	}
	
	
	@Override
	public void display() {
		System.out.println("Current condition: "+temp+" Celsius " + humidity + "% Humidity "+pressure + " Pressure");
	}

	

}
