package ObserverExample;

public interface DisplayElement {
	
	public void display();
	
}
