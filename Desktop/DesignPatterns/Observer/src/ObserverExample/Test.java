package ObserverExample;



public class Test {
	public static void main(String[]args)
	{
		WeatherData weatherData = new WeatherData();
		CurrentConditions currentCondition = new CurrentConditions(weatherData);
		StatisticsDisplay statistics = new StatisticsDisplay(weatherData);
		weatherData.setMeasurements(80, 65, 30.4f);
		weatherData.setMeasurements(86, 99, 30.4f);
		
	}
	
	
}
