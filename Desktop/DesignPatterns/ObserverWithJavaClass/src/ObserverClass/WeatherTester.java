package ObserverClass;

public class WeatherTester {

	public static void main(String[] args) {
		WeatherData weathercenter = new WeatherData();
		GeneralDisplay current = new GeneralDisplay(weathercenter);
		weathercenter.setMeasurements(30, 40, 30.6F);
	
	}

}
