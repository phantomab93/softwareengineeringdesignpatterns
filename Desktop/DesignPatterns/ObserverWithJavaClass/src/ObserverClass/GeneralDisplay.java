package ObserverClass;

import java.util.Observable;
import java.util.Observer;

public class GeneralDisplay implements Observer,DisplayInterface {

	Observable observable;
	private float temp;
	private float humidity;
	private float pressure;
	
	public GeneralDisplay(Observable observable){
		this.observable = observable;
		observable.addObserver(this);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof WeatherData){
			WeatherData weatherData = (WeatherData)o;
			temp = weatherData.getTemp();
			humidity = weatherData.getHumidity();
			pressure = weatherData.getPressure();
			display();
		}

	}




	@Override
	public void display() {
		System.out.print("Current Conditions: "+ temp + " F° Humidity: "+ humidity + "% pressure: "+ pressure);
	}

}
