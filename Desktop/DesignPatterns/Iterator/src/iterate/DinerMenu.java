package iterate;

import java.util.Iterator;

//Objective diner usa un array in modo che possa controllare il numero di entry nel menu
public class DinerMenu {
	
	static final int MAX_ITEMS = 6;
	MenuItems[] menuDiner;
	int numOfItems = 0;
	public DinerMenu(){
		menuDiner = new MenuItems[MAX_ITEMS];
		addItems("Vegitarian BLT", "BLT sandwich",true , 2.99);
		addItems("BLT", "BLT with bacon",false , 2.99);
		addItems("Soup of the day", "depends on the day",false , 3.29);
		addItems("Hotdog", "Hotdog with ketchup",true , 3.05);
	
	}
	
	public void addItems(String name, String description, boolean vegitarian, double price){	
		MenuItems menu = new MenuItems(name, description, vegitarian,price);
		if(numOfItems>=6)
			System.err.println("Out of Menu space");
		menuDiner[numOfItems] = menu;
		numOfItems++;
	}
	
	public Iterator createIterator(){
		return new DinerMenuIterator(menuDiner);
	}
	
	/* non serve più 
	public MenuItems[] getMenuItems(){
		return menuDiner;
	}
	
	*/
}
