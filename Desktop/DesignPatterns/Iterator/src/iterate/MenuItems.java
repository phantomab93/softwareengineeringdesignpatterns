package iterate;
//questo rappresenta un'implementazione comune tra objectvile diner e objectvile pancake house
public class MenuItems {
	
	String name;
	String description;
	boolean vegitarian;
	double price;
	
	public MenuItems(String name, String description, boolean vegitarian, double price)
	{
		this.name = name;
		this.description = description;
		this.vegitarian = vegitarian;
		this.price = price;
		
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
	public boolean getVegitarian(){
		return vegitarian;
	}
	
	public double getPrice(){
		return price;
	}
}
