package iterate;

import java.util.ArrayList;
import java.util.Iterator;

public class PancakeHouseMenuIterator implements Iterator {
	ArrayList<MenuItems> items;
	int position = 0;
 
	public PancakeHouseMenuIterator(ArrayList<MenuItems> items) {
		this.items = items;
	}
 
	public Object next() {
		MenuItems menuItem = items.get(position);
		position = position + 1;
		return menuItem;
	}
 
	public boolean hasNext() {
		if (position >= items.size()) {
			return false;
		} else {
			return true;
		}
	}
}