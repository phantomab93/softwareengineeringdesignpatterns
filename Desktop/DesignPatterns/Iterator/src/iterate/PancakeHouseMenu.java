package iterate;

import java.util.ArrayList;
import java.util.Iterator;

//pancakehouse usa un arraylist mentre objectville diner usa un array 
public class PancakeHouseMenu {
	ArrayList<MenuItems> menuItems;
	
	public PancakeHouseMenu(){
		menuItems = new ArrayList<MenuItems>();
		//inizializzazione del menu
		addItem("k&b pancake breakfast","pancake with scrambled eggs",true,2.99);
		addItem("Regular pancakes","pancake with sausage",false,2.99);
		addItem("Blueberry pancakes","pancake with freshblueberries",true,3.49);
		addItem("Waffles","with or without berries",true,3.59);
	}
	
	public void addItem(String name, String description, boolean vegitarian, double price ){
		MenuItems menu = new MenuItems(name,description,vegitarian,price);
		menuItems.add(menu);
	}
	
	
	public Iterator<?> createIterator()
	{
		return new PancakeHouseMenuIterator(menuItems);
	}
	
	/*
	public ArrayList<MenuItems> getMenuItems()
	{
		return menuItems;
	}
	
	*/
	
	
	
	
}
