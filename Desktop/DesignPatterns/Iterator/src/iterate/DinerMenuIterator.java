package iterate;

import java.util.Iterator;

public class DinerMenuIterator implements Iterator{
	MenuItems[] items;
	int position= 0; 
	
	public DinerMenuIterator(MenuItems[]items)
	{
		this.items=items;
	}
	
	@Override
	public boolean hasNext() {
		if(position >= items.length || items[position]==null)
			return false;
		return true;
	}

	@Override
	public Object next() {
		MenuItems item = items[position];
		position++;
		return item;
	}

}
