package iterate;

import java.util.ArrayList;
import java.util.Iterator;

public class Waitress {

		DinerMenu objectvileDiner;
		PancakeHouseMenu pancakeHouse;
		/* non serve più
		MenuItems[] menuOfDiner = objectvileDiner.getMenuItems();
		 non serve più
		ArrayList<MenuItems>menuOfPancakeHouse = pancakeHouse.getMenuItems();
		*/
		
		public Waitress(DinerMenu objectiveDiner, PancakeHouseMenu pancakeHouse)
		{
			this.objectvileDiner = objectiveDiner;
			this.pancakeHouse = pancakeHouse;
		}
		
		public void printMenu()
		{
			Iterator<?> pancakeIterator = pancakeHouse.createIterator();
			Iterator<?> dinerIterator = objectvileDiner.createIterator();
			System.out.println("PancakeHouse Menu:");
			printMenu(pancakeIterator);
			System.out.println("ObjectvileDiner Menu:");
			printMenu(dinerIterator);
		}
		
		public void printMenu(Iterator<?> menu)
		{
			while(menu.hasNext())
			{
				MenuItems menuItem = (MenuItems)menu.next();
				System.out.println("Name: "+ menuItem.getName() + " Description: "+ menuItem.getDescription()+ " Price: "+ menuItem.getPrice());
			}
		}
		
		/* stampa normale in questo caso anche se non è efficente

		// stampa pancakehousemenu without iterator

		for(int i = 0;i<menuOfPancakeHouse.size();i++)
		{
			MenuItems item = (MenuItems) menuOfPancakeHouse.get(i);
			System.out.println("menu item " + i + " of " + "pancakeHouse");
			System.out.println(item.getName() + " " + item.getDescription() + " " + item.getPrice());

		}	
		// stampa diner without iterator
		for(int k = 0;k < 4;k++)
		{
			MenuItems item = (MenuItems) menuOfDiner[k];
			System.out.println("menu item " + k + " of " + "Diner");
			System.out.println(item.getName() + " " + item.getDescription() + " " + item.getPrice());

		}
		*/

	
}
